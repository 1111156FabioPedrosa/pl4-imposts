/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package impostos;

/**
 *
 * @author fabio
 */
public class TrabContaOutrem extends ContribuinteTaxaRT {

    private String nomeEmpresa;

    public TrabContaOutrem(int rendimentosTrabalho, int outrosRendimentos, String nomeEmpresa, String nome, String morada) {
        super(rendimentosTrabalho, nome, morada, outrosRendimentos);
        setNomeEmpresa(nomeEmpresa);
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public final void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa.isEmpty() ? "sem nome" : nomeEmpresa;
    }

    @Override
    public double calcularImposto() {
        double taxa = this.getRendimentosTrabalho() <= 30000 ? 0.01 : 0.02;
        return this.getRendimentosTrabalho() * taxa + this.getOutrosRendimentos() * 0.02;
    }

    @Override
    public String toString() {
        return super.toString() + "\n"
                + "Imposto a pagar: " + calcularImposto() + "€.";
    }
}

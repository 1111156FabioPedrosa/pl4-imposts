/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package impostos;

/**
 *
 * @author fabio
 */
public class Reformado extends ContribuinteTaxaRT {

    public Reformado(int rendimentosTrabalho, String nome, String morada, double outrosRendimentos) {
        super(rendimentosTrabalho, nome, morada, outrosRendimentos);
    }

    @Override
    public double calcularImposto() {
        return this.getRendimentosTrabalho() * 0.01 + this.getOutrosRendimentos() * 0.03;
    }

    @Override
    public String toString() {
        return super.toString() + "\n"
                + "Imposto a pagar: " + calcularImposto() + "€.";
    }
}

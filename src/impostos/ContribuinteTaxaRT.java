/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package impostos;

/**
 *
 * @author fabio
 */
public abstract class ContribuinteTaxaRT extends Contribuinte {

    private int rendimentosTrabalho;

    public ContribuinteTaxaRT(int rendimentosTrabalho, String nome, String morada, double outrosRendimentos) {
        super(nome, morada, outrosRendimentos);
        setRendimentosTrabalho(rendimentosTrabalho);
    }

    public int getRendimentosTrabalho() {
        return rendimentosTrabalho;
    }

    public final void setRendimentosTrabalho(int rendimentosTrabalho) {
        this.rendimentosTrabalho = rendimentosTrabalho;
    }

    @Override
    public String toString() {
        return super.toString() + "\n"
                + "Rendimentos do trabalho: " + this.rendimentosTrabalho;
    }
}

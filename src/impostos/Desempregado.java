/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package impostos;

/**
 *
 * @author fabio
 */
public class Desempregado extends Contribuinte {

    private static float taxaOR = 2;
    private int mesesParagem;

    public Desempregado(int mesesParagem, String nome, String morada, double outrosRendimentos) {
        super(nome, morada, outrosRendimentos);
        this.mesesParagem = mesesParagem;
    }

    public int getMesesParagem() {
        return mesesParagem;
    }

    public void setMesesParagem(int mesesParagem) {
        this.mesesParagem = mesesParagem >= 0 ? mesesParagem : 0;
    }

    @Override
    public double calcularImposto() {
        return this.getOutrosRendimentos() * (taxaOR / 100);
    }

    @Override
    public String toString() {
        return super.toString() + "\n"
                + "Imposto a pagar: " + calcularImposto() + "€.";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package impostos;

/**
 *
 * @author fabio
 */
public abstract class Contribuinte {

    private String nome;
    private String morada;
    private double outrosRendimentos;

    public Contribuinte(String nome, String morada, double outrosRendimentos) {
        setNome(nome);
        setMorada(morada);
        setOutrosRendimentos(outrosRendimentos);
    }

    public String getMorada() {
        return morada;
    }

    public String getNome() {
        return nome;
    }

    public double getOutrosRendimentos() {
        return outrosRendimentos;
    }

    public final void setMorada(String morada) {
        if (morada.isEmpty()) {
            this.morada = "sem morada";
        } else {
            this.morada = morada;
        }
    }

    public final void setNome(String nome) {
        if (nome.isEmpty()) {
            this.nome = "sem nome";
        } else {
            this.nome = nome;
        }
    }

    public final void setOutrosRendimentos(double outrosRendimentos) {
        this.outrosRendimentos = outrosRendimentos >= 0 ? outrosRendimentos : 0;
    }

    @Override
    public String toString() {
        return "Contribuinte:\n"
                + "Nome: " + this.nome + "\n"
                + "Morada: " + this.morada + "\n"
                + "Outros rendimentos: " + this.outrosRendimentos;
    }

    public abstract double calcularImposto();
}

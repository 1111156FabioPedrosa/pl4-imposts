/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package impostos;

/**
 *
 * @author fabio
 */
public class TrabContaPropria extends ContribuinteTaxaRT {

    String proficao;

    public TrabContaPropria(int rendimentosTrabalho, String proficao, String nome, String morada, double outrosRendimentos) {
        super(rendimentosTrabalho, nome, morada, outrosRendimentos);
        setProficao(proficao);
    }

    public String getProficao() {
        return proficao;
    }

    public final void setProficao(String proficao) {
        this.proficao = proficao.isEmpty() ? "sem nome" : proficao;
    }

    @Override
    public double calcularImposto() {
        double taxa = this.getOutrosRendimentos() <= 50000 ? 0.02 : 0.05;
        return this.getRendimentosTrabalho() * 0.03 + this.getOutrosRendimentos() * taxa;
    }

    @Override
    public String toString() {
        return super.toString() + "\n"
                + "Imposto a pagar: " + calcularImposto() + "€.";
    }
}

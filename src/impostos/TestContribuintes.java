/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package impostos;

import java.util.ArrayList;

/**
 *
 * @author fabio
 */
public class TestContribuintes {
    public static void main(String[] args) {
        ArrayList<Contribuinte> contribuintes = new ArrayList<>();
        contribuintes.add(new Reformado(10000, "João", "Porto", 15000));
        contribuintes.add(new TrabContaOutrem(20000, 5000, "ISEP", "Tiago", "Matosinhos"));
        contribuintes.add(new TrabContaPropria(20000, "Programador", "Vitor", "Lisboa", 60000));
        contribuintes.add(new Desempregado(5, "Pedro", "Cascais", 15000));
        
        System.out.println("\nContribuintes:");
        for (Contribuinte c : contribuintes) {
            System.out.println("");
            System.out.println(c);
        }
        
        System.out.println("\nDesempregados:");
        for (Contribuinte c : contribuintes) {
            if (c instanceof Desempregado) {
                System.out.println("");
                System.out.println(c);
            }
        }
    }
}
